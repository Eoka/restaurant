﻿using GM.Core;
using GM.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GM.Menus.Controllers
{
    /// <summary>
    /// This class handle the main window events, no matter the underlying window rendering layer.
    /// It should be able to handle the following events :
    ///   - NewDishEvent triggered whenever the user ask for a new dish
    ///   - NewMenuEvent triggered whenever the user ask for a new menu
    ///   - NewSearchEvent triggered whenever the user type new text to search for
    ///   - QuitEvent triggered whenever the user want to quit the application
    /// </summary>
    public class MainWindowController : Controller
    {
        public interface MainWindow
        {
            MainWindowController Controller { get; set; }
            string SearchText { get; set; }
            IList<String> SearchResult { get; set; }

            void Close();
            void DisplayMessageBox(string text);
        }

        private MainWindow window;

        public MainWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                window.Controller = this;
            }
        }

        public void NewDishEvent()
        {
            RequestNavigationTo("EditDish", null);
        }

        public void NewMenuEvent()
        {
            RequestNavigationTo("EditMenu", null);
        }

        public void AccessOrderEvent()
        {
            RequestNavigationTo("Tables", null);
        }

        public void AccessHistoricEvent()
        {
            RequestNavigationTo("OrderHistory", null);
        }

        public void ItemOnClick(string text)
        {
            try
            {
                if (FileManager.MenusList.Contains(text))
                    RequestNavigationTo("EditMenu", FileManager.ReadMenu(FileManager.GetIdForName(text)));
                else
                    RequestNavigationTo("EditDish", FileManager.ReadDish(FileManager.GetIdForName(text)));
            }
            catch (Exception e)
            {
                Window.DisplayMessageBox("Erreur lors de la lecture du fichier : \n\t" + e.Message);
            }
        }

        public void NewSearchEvent()
        {
            window.SearchResult.Clear();
            foreach (var lst in FileManager.IndexValues)
                if (lst.Contains(window.SearchText))
                {
                    window.SearchResult.Add(lst);
                    window.SearchResult = window.SearchResult;
                }
                else
                    window.SearchResult.Clear();
        }

        public void QuitEvent()
        {
            FileManager.WriteIndex();
            Window.Close();
        }

        public override void HandleNavigation(object args)
        {

        }
    }
}
