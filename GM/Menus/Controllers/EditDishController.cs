﻿using GM.Core;
using GM.Models;
using GM.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GM.Menus.Controllers
{
    public class EditDishController : Controller
    {
        public interface EditDishWindow
        {
            EditDishController Controller { get; set; }
            string Name { get; set; }
            string ErrorForName { get; set; }

            decimal Price { get; set; }
            string ErrorForPrice { get; set; }

            IList<string> Ingredients { get; set; }
            string ErrorForIngredients { get; set; }
            IList<DishType> AvailableDishTypes { get; set; }
            string ErrorForDishType { get; set; }

            void Show();
            void Hide();
        }

        private EditDishWindow window;

        public EditDishWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                value.Controller = this;
            }
        }

        private bool isNew;
        private string oldName;
        private Dish editingDish;

        public EditDishController()
        {
            oldName = string.Empty;
        }

        public override void HandleNavigation(object args)
        {
            if(args != null && args is Dish)
            {
                editingDish = args as Dish;

                window.Name = editingDish.Name;
                window.Price = editingDish.Price;
                window.Ingredients = editingDish.Ingredients;
                window.AvailableDishTypes = editingDish.DishTypes;

                oldName = editingDish.Name;
                isNew = false;
            }
            else
                isNew = true;
            
            Window.Show();
        }


        public void SaveEvent()
        {
            if (FieldsAreValid())
            {
                if (isNew)
                {
                    Dish dish = new Dish(window.Name, window.Price, window.Ingredients.ToList(), window.AvailableDishTypes.ToList());
                    FileManager.WriteDish(dish);
                }
                else
                {
                    editingDish.Name = window.Name;
                    editingDish.Price = window.Price;
                    editingDish.Ingredients = window.Ingredients.ToList();
                    editingDish.DishTypes = window.AvailableDishTypes.ToList();

                    FileManager.EditDish(editingDish);
                }

                Window.Hide();
            }
        }

        private bool FieldsAreValid()
        {
            return NameIsValid() && PriceIsValid() && DishTypeIsValid();
        }

        private bool NameIsValid()
        {
            window.ErrorForName = "";
            bool isValid = true;

            if (string.IsNullOrEmpty(window.Name))
            {
                isValid = false;
                window.ErrorForName = "Le nom n'est pas valide";
            }
            else if (FileManager.IndexValues != null)
                foreach (string name in FileManager.IndexValues)
                    if (name.Equals(window.Name) && window.Name != oldName)
                    {
                        isValid = false;
                        window.ErrorForName = "Le nom existe déjà";
                        break;
                    }

            return isValid;
        }

        private bool PriceIsValid()
        {
            bool isValid = false;
            window.ErrorForPrice = "";

            if (window.Price <= 0)
                window.ErrorForPrice = "Le prix doit être plus grand que 0";
            else
                isValid = true;

            return isValid;
        }

        private bool DishTypeIsValid()
        {
            bool isValid = true;
            window.ErrorForDishType = "";

            if (window.AvailableDishTypes.Count > 1)
            {
                isValid = false;
                window.ErrorForDishType = "Vous devez sélectionner un type de plat";
            }

            return isValid;
        }

        public void WindowHasClosed()
        {
            Window = new Views.EditDishWindow();
        }

        public void CancelEvent()
        {
            Window.Hide();
        }
    }
}
