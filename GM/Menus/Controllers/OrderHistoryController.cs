﻿using GM.Core;
using GM.DataSources.EntityFramework;
using GM.Models.Orders;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Menus.Controllers
{
    public class OrderHistoryController : Controller
    {
        public interface OrderHistoryWindow
        {
            OrderHistoryController Controller { get; set; }
            void InitHistoricTable(IList data);
            void InitViewData(int orderAmount, float totalIncome, int averageTime);
            void Show();
        }

        private OrderHistoryWindow window;

        public OrderHistoryWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                window.Controller = this;
            }
        }

        private EFOrderMapper mapper;
        private List<Historic> historics;

        public override void HandleNavigation(object args)
        {
            mapper = new EFOrderMapper();
            InitWindow();
            Window.Show();
        }

        private void InitWindow()
        {
            InitHistorics();
            Window.InitHistoricTable(historics);
            int historicCount = 0;
            int orderAmount = 0;
            float totalIncome = 0;
            int averageTime = 0;

            foreach(Historic h in historics)
            {
                orderAmount += h.OrderAmount;
                totalIncome += h.TotalPrice;
                averageTime += h.AverageTime;

                if (h.OrderAmount != 0)
                    historicCount++;
            }

            Window.InitViewData(orderAmount, totalIncome, averageTime / (historicCount > 0 ? historicCount : 1));
        }

        private void InitHistorics()
        {
            historics = new List<Historic>();

            for (int i = 1; i < 7; i++)
            {
                Historic h = new Historic() { Table = i };
                historics.Add(h);
                mapper.GetTodayHistoric(h);
            }
        }

        public void WindowHasClosed()
        {
            Window = new Views.OrderHistoryWindow();
        }
    }
}
