﻿using GM.Core;
using GM.Models;
using GM.Utils;
using System;
using System.Collections.Generic;

namespace GM.Menus.Controllers
{
    public class EditMenuController : Controller
    {
        public interface EditMenuWindow
        {
            EditMenuController Controller { get; set; }
            string NameEntry { get; set; }
            string ErrorForNameEntry { get; set; }

            decimal Price { get; set; }
            string ErrorForPrice { get; set; }

            IDictionary<string, ICollection<string>> Dishes { get; set; }
            string ErrorForDishes { get; set; }

            IList<string> DishTypes { set; }
            IList<string> DishList { set; }

            void Show();
            void Hide();
        }

        private EditMenuWindow window;

        public EditMenuWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                value.Controller = this;
            }
        }
        
        private bool isNew;
        private string oldName;
        private Menu editingMenu;

        public EditMenuController()
        {
            oldName = string.Empty;
        }

        public override void HandleNavigation(object args)
        {
            if (args != null && args is Menu)
            {
                editingMenu = args as Menu;

                window.NameEntry = editingMenu.Name;
                window.Price = editingMenu.Price;
                window.Dishes = GetDishStringDictionary(editingMenu.Dishes);
                
                oldName = editingMenu.Name;
                isNew = false;
            }
            else
                isNew = true;

            window.DishTypes = InitDishTypes();
            Window.Show();
        }

        private IDictionary<string, ICollection<string>> GetDishStringDictionary(IDictionary<DishType, ICollection<Dish>> dishList)
        {
            IDictionary<string, ICollection<string>> dishDictionary = new Dictionary<string, ICollection<string>>();
            
            foreach(KeyValuePair<DishType, ICollection<Dish>> item in dishList)
            {
                dishDictionary.Add(DishTypeString.GetString(item.Key), new List<string>());
                foreach (Dish dish in item.Value)
                    dishDictionary[DishTypeString.GetString(item.Key)].Add(dish.Name);
            }

            return dishDictionary;
        }

        public void SaveEvent()
        {
            Dictionary<DishType, ICollection<Dish>> dishesList = GetDishList();

            if (FieldsAreValid(dishesList))
            {
                if (isNew)
                {
                    Menu menu = new Menu(window.NameEntry, window.Price, dishesList);
                    FileManager.WriteMenu(menu);
                }
                else
                {
                    editingMenu.Name = window.NameEntry;
                    editingMenu.Price = window.Price;
                    editingMenu.Dishes = GetDishList();

                    FileManager.EditMenu(editingMenu);
                }
                Window.Hide();
            }
        }

        private Dictionary<DishType, ICollection<Dish>> GetDishList()
        {
            Dictionary<DishType, ICollection<Dish>> dishesList = new Dictionary<DishType, ICollection<Dish>>();

            foreach (DishType dishType in Enum.GetValues(typeof(DishType)))
                dishesList.Add(dishType, new List<Dish>());

            foreach (KeyValuePair<string, ICollection<string>> item in window.Dishes)
                foreach (string dishName in item.Value)
                    dishesList[DishTypeString.GetDishType(item.Key)].Add(FileManager.ReadDish(FileManager.GetIdForName(dishName)));

            return dishesList;
        }

        private bool FieldsAreValid(Dictionary<DishType, ICollection<Dish>> d)
        {
            return NameIsValid() && PriceIsValid() && DishListIsValid(d);
        }

        private bool NameIsValid()
        {
            window.ErrorForNameEntry = "";
            bool isValid = true;

            if (string.IsNullOrEmpty(window.NameEntry))
            {
                window.ErrorForNameEntry = "Le nom n'est pas valide";
                isValid = false;
            }

            if (FileManager.IndexValues != null)
                foreach (string name in FileManager.IndexValues)
                    if (name.Equals(window.NameEntry) && window.NameEntry != oldName)
                    {
                        window.ErrorForNameEntry = "Le nom existe déjà";
                        isValid = false;
                    }
            
            return isValid;
        }

        private bool PriceIsValid()
        {
            window.ErrorForNameEntry = "";
            bool isValid = false;

            if (window.Price <= 0)
                window.ErrorForPrice = "Le prix doit être plus grand que 0";
            else
                isValid = true;

            return isValid;
        }

        private bool DishListIsValid(Dictionary<DishType, ICollection<Dish>> dishesPerType)
        {
            bool isValid = false;

            foreach (DishType dishType in dishesPerType.Keys)
                if (dishesPerType[dishType].Count > 0)
                {
                    isValid = true;
                    break;
                }

            if (isValid)
                window.ErrorForDishes = "";
            else
                window.ErrorForDishes = "Vous devez sélectionner un plat";
            
            return isValid;
        }

        private List<string> InitDishTypes()
        {
            List<string> dishTypes = new List<string>();
            foreach (DishType dishType in Enum.GetValues(typeof(DishType)))
            {
                dishTypes.Add(DishTypeString.GetString(dishType));
            }
            return dishTypes;
        }

        public void DishTypesField_SelectedValueChanged(string dishType)
        {
            if (dishType.Equals(DishTypeString.GetString(DishType.FirstDish)))
                window.DishList = FileManager.FirstDishesList;
            if (dishType.Equals(DishTypeString.GetString(DishType.MainDish)))
                window.DishList = FileManager.MainDishesList;
            if (dishType.Equals(DishTypeString.GetString(DishType.Dessert)))
                window.DishList = FileManager.DesertsList;
            if (dishType.Equals(DishTypeString.GetString(DishType.Drink)))
                window.DishList = FileManager.DrinksList;
        }

        public void CancelEvent()
        {
            Window.Hide();
        }

        public void WindowHasClosed()
        {
            Window = new Views.EditMenuWindow() { DishTypes = InitDishTypes() }; 
        }
    }
}
