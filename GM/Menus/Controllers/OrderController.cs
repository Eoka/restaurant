﻿using GM.Core;
using GM.DataSources.EntityFramework;
using GM.Menus.Views.Models;
using GM.Models;
using GM.Models.Orders;
using GM.Utils;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GM.Menus.Controllers
{
    public class OrderController : Controller
    {
        public interface OrderWindow
        {
            OrderController Controller { get; set; }
            string SearchText { get; set; }
            IList<string> SearchResult { get; set; }

            void BindDataTo(IList data);
            void SetTitle(int tableNumber);
            void AddItemToBinding(object item);
            void UpdateTotalPrice(float price);

            void Show();
            void Hide();
        }

        private OrderWindow window;
        public OrderWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                window.Controller = this;
            }
        }

        public event EventHandler orderCreated;

        protected void OnOrderCreated(object sender, EventArgs e)
        {
            if (orderCreated != null)
                orderCreated(sender, e);
        }

        private Table table;
        private Order order;
        private EFOrderMapper mapper;

        public override void HandleNavigation(object args)
        {
            if (args is Table)
            {
                table = args as Table;
                order = new Order() { TableNumber = table.TableId };
                mapper = new EFOrderMapper();

                Window.SetTitle(table.TableId);
                Window.BindDataTo(order.OrderLines);
            }
            Window.Show();
        }

        public void AddItem(string name)
        {
            MenuItem item = FileManager.ReadMenuItem(name);

            if (order[item.Id] != null)
                order[item.Id].Quantity++;
            else
                Window.AddItemToBinding(new OrderLine() { Item = item });

            Window.BindDataTo(order.OrderLines);
            Window.UpdateTotalPrice(order.TotalPrice);
        }

        public void CreateOrderEvent()
        {
            table.State = TableState.Taken;
            order.OrderOpening = DateTime.Now;

            mapper.Add(order);
            OnOrderCreated(this, null);

            window.Hide();
        }

        public void NewSearchEvent()
        {
            window.SearchResult.Clear();
            foreach (var lst in FileManager.IndexValues)
            {
                if (lst.Contains(window.SearchText))
                {
                    window.SearchResult.Add(lst);
                    window.SearchResult = window.SearchResult;
                }
                else
                    window.SearchResult.Clear();
            }
        }

        public void WindowHasClosed()
        {
            Window = new Views.OrderWindow();
        }
    }
}
