﻿using GM.Core;
using GM.DataSources.EntityFramework;
using GM.Menus.Views.Models;
using GM.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Menus.Controllers
{
    public class TablesController : Controller
    {
        public interface TablesWindow
        {
            TablesController Controller { get; set; }

            void UpdateButtons();

            void Show();
            void Hide();
        }

        private TablesWindow window;
        public TablesWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                window.Controller = this;
            }
        }

        private EFOrderMapper mapper;

        public override void HandleNavigation(object args)
        {
            mapper = new EFOrderMapper();
            Window.Show();
        }

        public void OnTableClick(Table table)
        {
            if (table.State == TableState.Free)
                RequestNavigationTo("Order", table);
            else
            {
                table.State = TableState.Free;
                mapper.CloseOrder(table);
            }
        }

        public void WindowHasClosed()
        {
            Window = new Views.TablesWindow();
        }

        public void RequestWindowUpdate(object sender, EventArgs e)
        {
            Window.UpdateButtons();
        }
    }
}
