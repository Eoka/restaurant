﻿namespace GM.Menus.Views
{
    partial class TablesWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.table1Button = new System.Windows.Forms.Button();
            this.table3Button = new System.Windows.Forms.Button();
            this.table2Button = new System.Windows.Forms.Button();
            this.table6Button = new System.Windows.Forms.Button();
            this.table5Button = new System.Windows.Forms.Button();
            this.table4Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // table1Button
            // 
            this.table1Button.BackColor = System.Drawing.SystemColors.Highlight;
            this.table1Button.FlatAppearance.BorderSize = 0;
            this.table1Button.ForeColor = System.Drawing.Color.White;
            this.table1Button.Location = new System.Drawing.Point(338, 32);
            this.table1Button.Name = "table1Button";
            this.table1Button.Size = new System.Drawing.Size(226, 122);
            this.table1Button.TabIndex = 0;
            this.table1Button.Text = "Table 1";
            this.table1Button.UseVisualStyleBackColor = false;
            this.table1Button.Click += new System.EventHandler(this.TableOnClick);
            // 
            // table2Button
            // 
            this.table2Button.BackColor = System.Drawing.SystemColors.Highlight;
            this.table2Button.FlatAppearance.BorderSize = 0;
            this.table2Button.ForeColor = System.Drawing.Color.White;
            this.table2Button.Location = new System.Drawing.Point(188, 32);
            this.table2Button.Name = "table2Button";
            this.table2Button.Size = new System.Drawing.Size(128, 122);
            this.table2Button.TabIndex = 2;
            this.table2Button.Text = "Table 2";
            this.table2Button.UseVisualStyleBackColor = false;
            this.table2Button.Click += new System.EventHandler(this.TableOnClick);
            // 
            // table3Button
            // 
            this.table3Button.BackColor = System.Drawing.SystemColors.Highlight;
            this.table3Button.FlatAppearance.BorderSize = 0;
            this.table3Button.ForeColor = System.Drawing.Color.White;
            this.table3Button.Location = new System.Drawing.Point(39, 32);
            this.table3Button.Name = "table3Button";
            this.table3Button.Size = new System.Drawing.Size(128, 122);
            this.table3Button.TabIndex = 1;
            this.table3Button.Text = "Table 3";
            this.table3Button.UseVisualStyleBackColor = false;
            this.table3Button.Click += new System.EventHandler(this.TableOnClick);
            // 
            // table4Button
            // 
            this.table4Button.BackColor = System.Drawing.SystemColors.Highlight;
            this.table4Button.FlatAppearance.BorderSize = 0;
            this.table4Button.ForeColor = System.Drawing.Color.White;
            this.table4Button.Location = new System.Drawing.Point(388, 199);
            this.table4Button.Name = "table4Button";
            this.table4Button.Size = new System.Drawing.Size(128, 122);
            this.table4Button.TabIndex = 5;
            this.table4Button.Text = "Table 4";
            this.table4Button.UseVisualStyleBackColor = false;
            this.table4Button.Click += new System.EventHandler(this.TableOnClick);
            // 
            // table5Button
            // 
            this.table5Button.BackColor = System.Drawing.SystemColors.Highlight;
            this.table5Button.FlatAppearance.BorderSize = 0;
            this.table5Button.ForeColor = System.Drawing.Color.White;
            this.table5Button.Location = new System.Drawing.Point(188, 199);
            this.table5Button.Name = "table5Button";
            this.table5Button.Size = new System.Drawing.Size(128, 122);
            this.table5Button.TabIndex = 4;
            this.table5Button.Text = "Table 5";
            this.table5Button.UseVisualStyleBackColor = false;
            this.table5Button.Click += new System.EventHandler(this.TableOnClick);
            // 
            // table6Button
            // 
            this.table6Button.BackColor = System.Drawing.SystemColors.Highlight;
            this.table6Button.FlatAppearance.BorderSize = 0;
            this.table6Button.ForeColor = System.Drawing.Color.White;
            this.table6Button.Location = new System.Drawing.Point(39, 199);
            this.table6Button.Name = "table6Button";
            this.table6Button.Size = new System.Drawing.Size(128, 122);
            this.table6Button.TabIndex = 3;
            this.table6Button.Text = "Table 6";
            this.table6Button.UseVisualStyleBackColor = false;
            this.table6Button.Click += new System.EventHandler(this.TableOnClick);
            // 
            // TablesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 362);
            this.Controls.Add(this.table4Button);
            this.Controls.Add(this.table5Button);
            this.Controls.Add(this.table6Button);
            this.Controls.Add(this.table2Button);
            this.Controls.Add(this.table3Button);
            this.Controls.Add(this.table1Button);
            this.Name = "TablesWindow";
            this.Text = "Helmo - Tables";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button table1Button;
        private System.Windows.Forms.Button table3Button;
        private System.Windows.Forms.Button table2Button;
        private System.Windows.Forms.Button table6Button;
        private System.Windows.Forms.Button table5Button;
        private System.Windows.Forms.Button table4Button;
    }
}