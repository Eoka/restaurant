﻿using GM.Menus.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GM.Menus.Views
{
    public partial class OrderHistoryWindow : Form, OrderHistoryController.OrderHistoryWindow
    {
        public OrderHistoryController Controller { get; set; }

        public OrderHistoryWindow()
        {
            InitializeComponent();
        }

        public void InitHistoricTable(IList data)
        {
            BindDataTo(data);
        }

        public void InitViewData(int orderAmount, float totalIncome, int averageTime)
        {
            orderAmountLabel.Text = orderAmount.ToString();
            incomeLabel.Text = totalIncome.ToString();
            averageTimeLabel.Text = averageTime.ToString();
        }

        private void BindDataTo(IList data)
        {
            bindingSource.DataSource = data;
            dataGridView.DataSource = bindingSource;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Controller.WindowHasClosed();
        }
    }
}
