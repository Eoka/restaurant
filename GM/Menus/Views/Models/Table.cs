﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Menus.Views.Models
{
    public enum TableState
    {
        Free,
        Reserved,
        Taken
    }

    public class Table
    {
        public int TableId { get; set; }
        public TableState State { get; set; }

        public Table(string tableName)
        {
            TableId = GetTableIdFromString(tableName);
            State = TableState.Free;
        }

        private int GetTableIdFromString(string tableName)
        {
            return int.Parse(tableName.Substring(5, Char.IsNumber(tableName[6]) ? 2 : 1));
        }
    }
}


