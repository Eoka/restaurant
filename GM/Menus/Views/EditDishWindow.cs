﻿using GM.Menus.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System;
using GM.Models;

namespace GM.Menus.Views
{
    public partial class EditDishWindow : Form, EditDishController.EditDishWindow
    {
        public EditDishWindow()
        {
            InitializeComponent();
        }

        public EditDishController Controller { get; set; }

        public new string Name
        {
            get { return NameField.Text; }
            set { NameField.Text = value; }
        }

        string EditDishController.EditDishWindow.ErrorForName
        {
            get { return ErrorForName.Text; }
            set { SetError(ErrorForName, value); }
        }

        public decimal Price
        {
            get { return PriceField.Value; }
            set { PriceField.Value = value; }
        }

        string EditDishController.EditDishWindow.ErrorForPrice
        {
            get { return ErrorForPrice.Text; }
            set { SetError(ErrorForPrice, value); }
        }

        public IList<string> Ingredients
        {
            get
            {
                if (!IngredientsField.Text.Split(new char[] { ',' })[0].Equals(string.Empty))
                    return IngredientsField.Text.Split(new char[] { ',' }).ToList();
                else
                    return new List<string>();
            }
            set
            {
                string result = "";

                foreach (string ingredient in value)
                {
                    result += ingredient;
                    if (!ingredient.Equals(value.Last()))
                        result += ",";
                }

                IngredientsField.Text = result;
            }
        }

        string EditDishController.EditDishWindow.ErrorForIngredients
        {
            get { return ErrorForIngredients.Text; }
            set { SetError(ErrorForIngredients, value); }
        }

        string EditDishController.EditDishWindow.ErrorForDishType
        {
            get { return ErrorForDishType.Text; }
            set { SetError(ErrorForDishType, value); }
        }

        IList<DishType> EditDishController.EditDishWindow.AvailableDishTypes
        {
            get
            {
                List<DishType> items = new List<DishType>();
                foreach (string type in availableDishTypesCheckBox.CheckedItems)
                {
                    if (type.Equals("Entrée"))
                        items.Add(DishType.FirstDish);
                    if (type.Equals("Plat"))
                        items.Add(DishType.MainDish);
                    if (type.Equals("Dessert"))
                        items.Add(DishType.Dessert);
                    if (type.Equals("Boisson"))
                        items.Add(DishType.Drink);
                }
                return items;
            }
            set
            {
                availableDishTypesCheckBox.SetItemChecked(0, value.Contains(DishType.FirstDish));
                availableDishTypesCheckBox.SetItemChecked(1, value.Contains(DishType.MainDish));
                availableDishTypesCheckBox.SetItemChecked(2, value.Contains(DishType.Dessert));
                availableDishTypesCheckBox.SetItemChecked(3, value.Contains(DishType.Drink));
            }
        }

        private void SaveClick(object sender, EventArgs e)
        {
            Controller.SaveEvent();
        }

        private void CancelClick(object sender, EventArgs e)
        {
            Controller.CancelEvent();
        }

        private void SetError(Label destination, string value)
        {
            destination.Text = value;
            destination.Visible = !string.IsNullOrWhiteSpace(value);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Controller.WindowHasClosed();
        }
    }
}
