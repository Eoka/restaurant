﻿using System.Windows.Forms;

namespace GM.Menus.Views
{
    partial class EditDishWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NameField = new System.Windows.Forms.TextBox();
            this.PriceField = new System.Windows.Forms.NumericUpDown();
            this.ErrorForName = new System.Windows.Forms.Label();
            this.ErrorForPrice = new System.Windows.Forms.Label();
            this.IngredientsField = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ErrorForIngredients = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Save = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.availableDishTypesCheckBox = new System.Windows.Forms.CheckedListBox();
            this.availableDishTypesLabel = new System.Windows.Forms.Label();
            this.ErrorForDishType = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PriceField)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.NameField, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.PriceField, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ErrorForName, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.ErrorForPrice, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.IngredientsField, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.ErrorForIngredients, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.availableDishTypesCheckBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.availableDishTypesLabel, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 13);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(726, 438);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom : ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(233, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prix en euros : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NameField
            // 
            this.NameField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameField.Location = new System.Drawing.Point(242, 3);
            this.NameField.Name = "NameField";
            this.NameField.Size = new System.Drawing.Size(298, 20);
            this.NameField.TabIndex = 3;
            // 
            // PriceField
            // 
            this.PriceField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.PriceField.AutoSize = true;
            this.PriceField.Location = new System.Drawing.Point(242, 35);
            this.PriceField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PriceField.Name = "PriceField";
            this.PriceField.Size = new System.Drawing.Size(41, 20);
            this.PriceField.TabIndex = 4;
            this.PriceField.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ErrorForName
            // 
            this.ErrorForName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ErrorForName.AutoSize = true;
            this.ErrorForName.ForeColor = System.Drawing.Color.Red;
            this.ErrorForName.Location = new System.Drawing.Point(546, 0);
            this.ErrorForName.Name = "ErrorForName";
            this.ErrorForName.Size = new System.Drawing.Size(177, 13);
            this.ErrorForName.TabIndex = 6;
            this.ErrorForName.Visible = false;
            // 
            // ErrorForPrice
            // 
            this.ErrorForPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ErrorForPrice.AutoSize = true;
            this.ErrorForPrice.ForeColor = System.Drawing.Color.Red;
            this.ErrorForPrice.Location = new System.Drawing.Point(546, 32);
            this.ErrorForPrice.Name = "ErrorForPrice";
            this.ErrorForPrice.Size = new System.Drawing.Size(177, 13);
            this.ErrorForPrice.TabIndex = 7;
            this.ErrorForPrice.Visible = false;
            // 
            // IngredientsField
            // 
            this.IngredientsField.AcceptsReturn = true;
            this.IngredientsField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IngredientsField.Location = new System.Drawing.Point(242, 67);
            this.IngredientsField.Multiline = true;
            this.IngredientsField.Name = "IngredientsField";
            this.IngredientsField.Size = new System.Drawing.Size(298, 256);
            this.IngredientsField.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(233, 262);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ingrédients (tapez les noms séparés par des virgules)";
            // 
            // ErrorForIngredients
            // 
            this.ErrorForIngredients.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ErrorForIngredients.AutoSize = true;
            this.ErrorForIngredients.ForeColor = System.Drawing.Color.Red;
            this.ErrorForIngredients.Location = new System.Drawing.Point(546, 64);
            this.ErrorForIngredients.Name = "ErrorForIngredients";
            this.ErrorForIngredients.Size = new System.Drawing.Size(177, 262);
            this.ErrorForIngredients.TabIndex = 8;
            this.ErrorForIngredients.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.Save);
            this.flowLayoutPanel1.Controls.Add(this.Cancel);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(546, 409);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(177, 26);
            this.flowLayoutPanel1.TabIndex = 10;
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Save.Location = new System.Drawing.Point(3, 3);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 10;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.SaveClick);
            // 
            // Cancel
            // 
            this.Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Cancel.Location = new System.Drawing.Point(84, 3);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 9;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.CancelClick);
            // 
            // availableDishTypesCheckBox
            // 
            this.availableDishTypesCheckBox.FormattingEnabled = true;
            this.availableDishTypesCheckBox.Items.AddRange(new object[] {
            "Entrée",
            "Plat",
            "Dessert",
            "Boisson"});
            this.availableDishTypesCheckBox.Location = new System.Drawing.Point(242, 329);
            this.availableDishTypesCheckBox.Name = "availableDishTypesCheckBox";
            this.availableDishTypesCheckBox.Size = new System.Drawing.Size(298, 64);
            this.availableDishTypesCheckBox.TabIndex = 11;
            // 
            // availableDishTypesLabel
            // 
            this.availableDishTypesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableDishTypesLabel.AutoSize = true;
            this.availableDishTypesLabel.Location = new System.Drawing.Point(3, 326);
            this.availableDishTypesLabel.Name = "availableDishTypesLabel";
            this.availableDishTypesLabel.Size = new System.Drawing.Size(233, 80);
            this.availableDishTypesLabel.TabIndex = 14;
            this.availableDishTypesLabel.Text = "Disponible comme :";
            // 
            // ErrorForDishType
            // 
            this.ErrorForDishType.AutoSize = true;
            this.ErrorForDishType.ForeColor = System.Drawing.Color.Red;
            this.ErrorForDishType.Location = new System.Drawing.Point(546, 326);
            this.ErrorForDishType.Name = "ErrorForDishType";
            this.ErrorForDishType.Size = new System.Drawing.Size(0, 13);
            this.ErrorForDishType.TabIndex = 15;
            // 
            // EditDishWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 463);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "EditDishWindow";
            this.Text = "Edition d\'un plat";
            this.TopMost = true;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PriceField)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NameField;
        private System.Windows.Forms.NumericUpDown PriceField;
        private System.Windows.Forms.TextBox IngredientsField;
        private System.Windows.Forms.Label ErrorForName;
        private System.Windows.Forms.Label ErrorForPrice;
        private System.Windows.Forms.Label ErrorForIngredients;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.CheckedListBox availableDishTypesCheckBox;
        private System.Windows.Forms.Label availableDishTypesLabel;
        private Label ErrorForDishType;
    }
}