﻿using GM.Menus.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GM.Models.Orders;

namespace GM.Menus.Views
{
    public partial class OrderWindow : Form, OrderController.OrderWindow
    {
        public OrderController Controller { get; set; }
        private IList<string> searchResult;
        
        public OrderWindow()
        {
            searchResult = new List<string>();
            InitializeComponent();
        }

        private void SearchFieldOnTextChanged(object sender, EventArgs e)
        {
            Controller.NewSearchEvent();
        }

        public string SearchText
        {
            get { return searchField.Text; }
            set { searchField.Text = value; }
        }

        public IList<string> SearchResult
        {
            get { return searchResult; }
            set
            {
                searchResultField.Items.Clear();
                foreach (string result in value)
                    searchResultField.Items.Add(result);
            }
        }

        public void SearchResultFieldItemActivate(object sender, EventArgs e)
        {
            foreach (ListViewItem item in searchResultField.SelectedItems)
                Controller.AddItem(item.Text);
        }

        public void SetTitle(int tableNumber)
        {
            Text = "Helmo - Table " + tableNumber;
        }

        public void BindDataTo(IList data)
        {
            bindingSource.DataSource = data;
            bindingSource.ResetBindings(true);
            dataGridView.DataSource = bindingSource;
        }

        public void AddItemToBinding(object item)
        {
            bindingSource.Add(item);
        }

        private void CreateButtonOnClick(object sender, EventArgs e)
        {
            Controller.CreateOrderEvent();
        }

        public void UpdateTotalPrice(float price)
        {
            totalPriceLabel.Text = price.ToString();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Controller.WindowHasClosed();
        }
    }
}
