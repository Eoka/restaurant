﻿using GM.Menus.Controllers;
using GM.Models;
using GM.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GM.Menus.Views
{
    public partial class EditMenuWindow : Form, EditMenuController.EditMenuWindow
    {
        public EditMenuWindow()
        {
            InitializeComponent();
            dishes = new Dictionary<string, ICollection<string>>();

            foreach (DishType dishType in Enum.GetValues(typeof(DishType)))
                dishes.Add(DishTypeString.GetString(dishType), new List<string>());
        }

        public EditMenuController Controller { get; set; }

        public string NameEntry
        {
            get { return nameField.Text; }
            set { nameField.Text = value; }
        }

        public string ErrorForNameEntry
        {
            get { return errorForName.Text; }
            set { SetError(errorForName, value); }
        }

        public decimal Price
        {
            get { return priceField.Value; }
            set { priceField.Value = value; }
        }

        public string ErrorForPrice
        {
            get { return errorForPrice.Text; }
            set { SetError(errorForPrice, value); }
        }

        private IDictionary<string, ICollection<string>> dishes;

        public IDictionary<string, ICollection<string>> Dishes
        {
            get { return dishes; }
            set
            {
                dishes = value;
                UpdateAddedDishesField();
            }
        }

        public string ErrorForDishes
        {
            get { return errorForDishes.Text; }
            set { SetError(errorForDishes, value); }
        }

        public IList<string> DishTypes
        {
            set
            {
                dishTypesField.Items.Clear();
                foreach (string val in value)
                    dishTypesField.Items.Add(val);
            }
        }

        public IList<string> DishList
        {
            set
            {
                dishesField.Items.Clear();
                foreach (string val in value)
                    dishesField.Items.Add(val);
            }
        }

        private void SetError(Label destination, string value)
        {
            destination.Text = value;
            destination.Visible = !string.IsNullOrWhiteSpace(value);
        }

        private void AddDishClick(object sender, EventArgs e)
        {
            dishes[dishTypesField.SelectedItem as string].Add(dishesField.SelectedItem as string);
            UpdateAddedDishesField();
        }

        private void UpdateAddedDishesField()
        {
            AddedDishesField.Clear();
            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, ICollection<string>> dishPerType in dishes)
                if (dishPerType.Value.Count > 0)
                {
                    sb.AppendFormat("{0}\r\n", dishPerType.Key);
                    foreach (string d in dishPerType.Value)
                        sb.AppendFormat("\t{0}\r\n", d);
                }
            
            AddedDishesField.Text = sb.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Controller.SaveEvent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Controller.CancelEvent();
        }

        private void DishTypesField_SelectedValueChanged(object sender, EventArgs e)
        {
            Controller.DishTypesField_SelectedValueChanged(dishTypesField.SelectedItem.ToString());
            dishesField.Enabled = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Controller.WindowHasClosed();
        }
    }
}
