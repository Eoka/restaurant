﻿using GM.Menus.Controllers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GM.Menus.Views
{
    public partial class MainWindow : Form, MainWindowController.MainWindow
    {
        public MainWindowController Controller { get; set; }

        private IList<string> searchResult;

        public MainWindow()
        {
            searchResult = new List<string>();
            InitializeComponent();
        }

        private void SearchFieldOnTextChanged(object sender, EventArgs e)
        {
            Controller.NewSearchEvent();
        }

        public string SearchText
        {
            get { return searchField.Text; }
            set { searchField.Text = value; }
        }

        public IList<string> SearchResult
        {
            get { return searchResult; }
            set
            {
                searchResultField.Items.Clear();
                foreach (string result in value)
                    searchResultField.Items.Add(result);
            }
        }

        public void SearchResultFieldItemActivate(object sender, EventArgs e)
        {
            foreach (ListViewItem item in searchResultField.SelectedItems)
                Controller.ItemOnClick(item.Text);
        }

        private void NewDishToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            Controller.NewDishEvent();
        }

        private void NewMenuToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            Controller.NewMenuEvent();
        }

        private void OrderToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            Controller.AccessOrderEvent();
        }

        private void HistoricToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Controller.AccessHistoricEvent();
        }

        private void ExitToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            Controller.QuitEvent();
        }

        public void DisplayMessageBox(string message)
        {
            MessageBox.Show(message);
        }
    }
}
