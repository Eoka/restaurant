﻿namespace GM.Menus.Views
{
    partial class EditMenuWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.AddedDishesField = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.errorForName = new System.Windows.Forms.Label();
            this.errorForPrice = new System.Windows.Forms.Label();
            this.errorForDishes = new System.Windows.Forms.Label();
            this.nameField = new System.Windows.Forms.TextBox();
            this.priceField = new System.Windows.Forms.NumericUpDown();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.dishTypesField = new System.Windows.Forms.ComboBox();
            this.dishesField = new System.Windows.Forms.ComboBox();
            this.addDish = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.priceField)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.AddedDishesField, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.errorForName, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.errorForPrice, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.errorForDishes, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.nameField, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.priceField, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.addDish, 2, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(597, 330);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // AddedDishesField
            // 
            this.AddedDishesField.AcceptsReturn = true;
            this.AddedDishesField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddedDishesField.Enabled = false;
            this.AddedDishesField.Location = new System.Drawing.Point(122, 99);
            this.AddedDishesField.Multiline = true;
            this.AddedDishesField.Name = "AddedDishesField";
            this.AddedDishesField.Size = new System.Drawing.Size(292, 194);
            this.AddedDishesField.TabIndex = 12;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(420, 299);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(174, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Sauver";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(84, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Annuler";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nom :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prix en euros :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Ajouter un plat :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Plats ajoutés";
            // 
            // errorForName
            // 
            this.errorForName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorForName.AutoSize = true;
            this.errorForName.ForeColor = System.Drawing.Color.Red;
            this.errorForName.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.errorForName.Location = new System.Drawing.Point(420, 0);
            this.errorForName.Name = "errorForName";
            this.errorForName.Size = new System.Drawing.Size(174, 32);
            this.errorForName.TabIndex = 5;
            this.errorForName.Text = "label5";
            this.errorForName.Visible = false;
            // 
            // errorForPrice
            // 
            this.errorForPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorForPrice.AutoSize = true;
            this.errorForPrice.ForeColor = System.Drawing.Color.Red;
            this.errorForPrice.Location = new System.Drawing.Point(420, 32);
            this.errorForPrice.Name = "errorForPrice";
            this.errorForPrice.Size = new System.Drawing.Size(174, 32);
            this.errorForPrice.TabIndex = 6;
            this.errorForPrice.Text = "label5";
            this.errorForPrice.Visible = false;
            // 
            // errorForDishes
            // 
            this.errorForDishes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorForDishes.AutoSize = true;
            this.errorForDishes.ForeColor = System.Drawing.Color.Red;
            this.errorForDishes.Location = new System.Drawing.Point(420, 96);
            this.errorForDishes.Name = "errorForDishes";
            this.errorForDishes.Size = new System.Drawing.Size(174, 200);
            this.errorForDishes.TabIndex = 7;
            this.errorForDishes.Text = "label5";
            this.errorForDishes.Visible = false;
            // 
            // nameField
            // 
            this.nameField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameField.Location = new System.Drawing.Point(122, 3);
            this.nameField.Name = "nameField";
            this.nameField.Size = new System.Drawing.Size(292, 20);
            this.nameField.TabIndex = 8;
            // 
            // priceField
            // 
            this.priceField.Location = new System.Drawing.Point(122, 35);
            this.priceField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.priceField.Name = "priceField";
            this.priceField.Size = new System.Drawing.Size(62, 20);
            this.priceField.TabIndex = 9;
            this.priceField.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.Controls.Add(this.dishTypesField);
            this.flowLayoutPanel2.Controls.Add(this.dishesField);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(122, 67);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(292, 26);
            this.flowLayoutPanel2.TabIndex = 10;
            // 
            // dishTypesField
            // 
            this.dishTypesField.FormattingEnabled = true;
            this.dishTypesField.Location = new System.Drawing.Point(3, 3);
            this.dishTypesField.Name = "dishTypesField";
            this.dishTypesField.Size = new System.Drawing.Size(121, 21);
            this.dishTypesField.TabIndex = 0;
            this.dishTypesField.SelectedValueChanged += new System.EventHandler(this.DishTypesField_SelectedValueChanged);
            // 
            // dishesField
            // 
            this.dishesField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dishesField.Enabled = false;
            this.dishesField.FormattingEnabled = true;
            this.dishesField.Location = new System.Drawing.Point(130, 3);
            this.dishesField.Name = "dishesField";
            this.dishesField.Size = new System.Drawing.Size(151, 21);
            this.dishesField.TabIndex = 1;
            // 
            // addDish
            // 
            this.addDish.Location = new System.Drawing.Point(420, 67);
            this.addDish.Name = "addDish";
            this.addDish.Size = new System.Drawing.Size(75, 23);
            this.addDish.TabIndex = 11;
            this.addDish.Text = "Ajouter";
            this.addDish.UseVisualStyleBackColor = true;
            this.addDish.Click += new System.EventHandler(this.AddDishClick);
            // 
            // EditMenuWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 354);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "EditMenuWindow";
            this.Text = "Edition d\'un menu";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.priceField)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label errorForName;
        private System.Windows.Forms.Label errorForPrice;
        private System.Windows.Forms.Label errorForDishes;
        private System.Windows.Forms.TextBox nameField;
        private System.Windows.Forms.NumericUpDown priceField;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ComboBox dishTypesField;
        private System.Windows.Forms.ComboBox dishesField;
        private System.Windows.Forms.Button addDish;
        private System.Windows.Forms.TextBox AddedDishesField;
    }
}