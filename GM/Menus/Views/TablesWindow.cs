﻿using GM.Menus.Controllers;
using GM.Menus.Views.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GM.Menus.Views
{
    public partial class TablesWindow : Form, TablesController.TablesWindow
    {
        private Dictionary<Button, Table> tableButtonMapping;

        public TablesWindow()
        {
            InitializeComponent();
            tableButtonMapping = new Dictionary<Button, Table>();

            tableButtonMapping.Add(table1Button, new Table(table1Button.Name));
            tableButtonMapping.Add(table2Button, new Table(table2Button.Name));
            tableButtonMapping.Add(table3Button, new Table(table3Button.Name));
            tableButtonMapping.Add(table4Button, new Table(table4Button.Name));
            tableButtonMapping.Add(table5Button, new Table(table5Button.Name));
            tableButtonMapping.Add(table6Button, new Table(table6Button.Name));
        }

        public TablesController Controller { get; set; }

        private void TableOnClick(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button b = sender as Button;
                Controller.OnTableClick(tableButtonMapping[b]);
                UpdateButtons();
            }
        }

        public void UpdateButtons()
        {
            foreach (KeyValuePair<Button, Table> item in tableButtonMapping)
                UpdateButton(item.Key);
        }

        private void UpdateButton(Button b)
        {
            if (tableButtonMapping[b].State == TableState.Free)
                b.BackColor = SystemColors.Highlight;
            else
                b.BackColor = Color.Orange;
        }

        public void DisplayMessageBox(string message)
        {
            MessageBox.Show(message);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            foreach(Table t in tableButtonMapping.Values)
                if (t.State != TableState.Free)
                {
                    e.Cancel = true;
                    DisplayMessageBox("Au moins une commande n'a pas été fermée.\nVeuillez fermer la commande avant de quitter la fenêtre");
                    break;
                }
            
            base.OnFormClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Controller.WindowHasClosed();
        }

    }
}
