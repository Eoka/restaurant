﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Models.Orders
{
    public class Order
    {
        public long Id { get; set; }
        public int TableNumber { get; set; }
        public List<OrderLine> OrderLines { get; set; }
        public DateTime OrderOpening { get; set; }
        public DateTime OrderClosing { get; set; }

        public float TotalPrice
        {
            get
            {
                float price = 0;
                foreach (OrderLine ol in OrderLines)
                    price += ol.TotalPrice;

                return price;
            }
        }

        public OrderLine this[long id]
        {
            get
            {
                foreach (OrderLine ol in OrderLines)
                    if (ol.ItemAdress == id)
                        return ol;

                return null;
            }
        }

        public Order()
        {
            TableNumber = -1;
            OrderLines = new List<OrderLine>();
            OrderOpening = DateTime.Now;
            OrderClosing = DateTime.Today.AddDays(-1);
        }

        public bool ContainsItem(long id)
        {
            foreach (OrderLine ol in OrderLines)
                if (ol.ItemAdress == id)
                    return true;

            return false;
        }
    }
}
