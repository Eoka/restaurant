﻿using GM.Menus.Views.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Models.Orders
{
    public class Historic
    {
        public int Table { get; set; }
        public int OrderAmount { get; set; }
        public float TotalPrice { get; set; }
        public int AverageTime { get; set; }
        
        public Historic()
        {
            OrderAmount = 0;
            TotalPrice = 0;
            AverageTime = 0;
        }
    }
}
