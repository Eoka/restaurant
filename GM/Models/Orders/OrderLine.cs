﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Models.Orders
{
    public class OrderLine
    {
        private MenuItem item;
        
        [Browsable(false)]
        public long Id { get; set; }
        [Browsable(false)]
        public MenuItem Item { get; set; }
        [Browsable(false)]
        public long ItemAdress { get { return Item.Id; } set { Item.Id = value; } }

        public string ItemName { get { return Item.Name; } }
        public int Quantity { get; set; }
        public float UnitPrice { get { return (float)Item.Price; } set { Item.Price = (decimal)value; } }
        public float TotalPrice { get { return Quantity * UnitPrice; } }

        public OrderLine()
        {
            Quantity = 1;
            Item = new MenuItem();
        }
    }
}
