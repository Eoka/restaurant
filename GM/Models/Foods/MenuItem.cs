﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Models
{
    public class MenuItem
    {
        private long id;
        private string name;
        private decimal price;

        public long Id { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public decimal Price { get { return price; } set { price = value; } }

        public MenuItem() { }

        protected MenuItem(long id, string name, decimal price)
        {
            this.id = id;
            this.name = name;
            this.price = price;
        }

        protected MenuItem(string name, decimal price) : this(DefineId(), name, price) { }

        private static long DefineId() { return Int64.Parse(DateTime.Now.ToString("yyyyMMddHHmmssffff")); }
    }
}
