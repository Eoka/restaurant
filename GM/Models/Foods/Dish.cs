﻿using GM.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Models
{
    public enum DishType
    {
        FirstDish,
        MainDish,
        Dessert,
        Drink
    }

    public static class DishTypeString
    {
        public static string GetString(DishType dishType)
        {
            if (dishType.Equals(DishType.FirstDish))
                return "Entrée";
            if (dishType.Equals(DishType.MainDish))
                return "Plat";
            if (dishType.Equals(DishType.Dessert))
                return "Dessert";
            if (dishType.Equals(DishType.Drink))
                return "Boisson";

            return null;
        }
        
        public static DishType GetDishType(string name)
        {
            if (name.Equals("Entrée"))
                return DishType.FirstDish;
            if (name.Equals("Plat"))
                return DishType.MainDish;
            if (name.Equals("Dessert"))
                return DishType.Dessert;
            if (name.Equals("Boisson"))
                return DishType.Drink;

            throw new Exception("DishType does not exists");
        }
    }

    public class Dish : MenuItem
    {
        private List<string> ingredients;
        private List<DishType> dishTypes;

        public List<string> Ingredients { get { return ingredients; } set { ingredients = value; } }
        public List<DishType> DishTypes { get { return dishTypes; } set { dishTypes = value; } }

        public Dish(long id, string name, decimal price, List<string> ingredients, List<DishType> dishTypes) : base(id, name, price)
        {
            this.ingredients = ingredients;
            this.dishTypes = dishTypes;
        }


        public Dish(long id, string name, decimal price, List<string> ingredients) : base(id, name, price)
        {
            this.ingredients = ingredients;
            dishTypes = new List<DishType>();
        }

        public Dish(string name, decimal price, List<string> ingredients, List<DishType> dishTypes) : base(name, price)
        {
            this.ingredients = ingredients;
            this.dishTypes = dishTypes;
        }
    }
}
