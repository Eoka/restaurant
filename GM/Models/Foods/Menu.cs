﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Models
{
    public class Menu : MenuItem
    {
        private IDictionary<DishType, ICollection<Dish>> dishes;

        public IDictionary<DishType, ICollection<Dish>> Dishes { get { return dishes; } set { dishes = value; } }

        public Menu(long id, string name, decimal price, IDictionary<DishType, ICollection<Dish>> dishes) : base(id, name, price)
        {
            this.dishes = dishes;
        }

        public Menu(string name, decimal price, IDictionary<DishType, ICollection<Dish>> dishes) : base(name, price)
        {
            this.dishes = dishes;
        }
    }
}
