﻿using GM.Utils;

namespace GM.Core
{
    /// <summary>
    /// Describe an abstract controller.
    /// If you want to benefit from the navigation facilities : your new controller should
    /// inherit this one.
    /// </summary>
    public abstract class Controller
    {
        public IApplicationController AppController { get; set; }

        public FileManager FileManager { get; set; }
        
        /// <summary>
        /// Allows you to send a message to another controller
        /// without knowing it.
        /// </summary>
        /// <param name="name">Datas usefull the targeted controller.</param>
        public void RequestNavigationTo(string name, object args) 
        {
            AppController.NavigateTo(name, args);
        }

        /// <summary>
        /// Override this method to handle navigation event.
        /// </summary>
        /// <param name="args">
        /// reprensent some argument usefull to handle the event.
        /// You should cast this args to an expected type of event args.
        /// </param>
        public abstract void HandleNavigation(object args);    
    }

}
