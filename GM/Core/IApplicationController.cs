﻿using System;
namespace GM.Core
{
    public interface IApplicationController
    {
        System.Collections.Generic.ICollection<string> ControllersName { get; }
        void NavigateTo(string controllerName, object args);
        void Register(Controller controller);
    }
}
