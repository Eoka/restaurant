﻿using GM.Models;
using GM.Utils.Index;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GM.Utils
{
    public class MenuFileManager : SpecializedManager
    {
        override public void Write(MenuItem menu)
        {
            try
            {
                using (Writer = File.CreateText(FileManager.Path + menu.Id))
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine("Menu");
                    builder.AppendLine("\tId:" + menu.Id);
                    builder.AppendLine("\tName:" + menu.Name);
                    builder.AppendLine("\tPrice:" + menu.Price);

                    foreach (KeyValuePair<DishType, ICollection<Dish>> dishType in (menu as Menu).Dishes)
                    {
                        builder.AppendLine("\t" + DishTypeString.GetString(dishType.Key) + "s:");
                        if (dishType.Value.Count > 0)
                            foreach (Dish dish in dishType.Value)
                                builder.AppendLine("\t\t" + dish.Id);
                        else
                            builder.AppendLine("\t\t");
                    }

                    Writer.Write(builder.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        override public MenuItem Read(long id)
        {
            MenuItem menu;
            try
            {
                using (Reader = File.OpenText(FileManager.Path + id))
                {
                    string name = string.Empty;
                    decimal price = 0;

                    IDictionary<DishType, ICollection<Dish>> dishes = new Dictionary<DishType, ICollection<Dish>>();

                    foreach (DishType dishType in Enum.GetValues(typeof(DishType)))
                        dishes.Add(dishType, new List<Dish>());

                    string buffer = Reader.ReadLine();

                    while (buffer != null)
                    {
                        if (buffer.Contains("\tName:"))
                            name = GetValue(buffer);
                        else if (buffer.Contains("\tPrice:"))
                            price = decimal.Parse(GetValue(buffer));
                        else if (buffer.Contains("\tEntrées:"))
                            AddDishesToCorrespondingDictionary(buffer, dishes, DishType.FirstDish);
                        else if (buffer.Contains("\tPlats:"))
                            AddDishesToCorrespondingDictionary(buffer, dishes, DishType.MainDish);
                        else if (buffer.Contains("\tDesserts:"))
                            AddDishesToCorrespondingDictionary(buffer, dishes, DishType.Dessert);
                        else if (buffer.Contains("\tBoissons:"))
                            AddDishesToCorrespondingDictionary(buffer, dishes, DishType.Drink);

                        if (buffer != null)
                            buffer = Reader.ReadLine();
                    }
                    
                    menu = new Menu(id, name, price, dishes);
                    return menu;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void AddDishesToCorrespondingDictionary(string buffer, IDictionary<DishType, ICollection<Dish>> dictionary, DishType dictionaryKey)
        {
            buffer = Reader.ReadLine();
            while (buffer != null && buffer.StartsWith("\t\t") && buffer.Length > 2)
            {
                dictionary[dictionaryKey].Add(GetDishFromBuffer(buffer));
                buffer = Reader.ReadLine();
            }
        }

        private Dish GetDishFromBuffer(string buffer)
        {
            return FileManager.ReadDish(long.Parse(GetValue(buffer)));
        }
    }
}
