﻿using GM.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Utils
{
    public abstract class SpecializedManager
    {
        public TextReader Reader { get; set; }
        public TextWriter Writer { get; set; }
        public FileManager FileManager { get; set; }
        
        public abstract void Write(MenuItem item);
        public abstract MenuItem Read(long id);

        protected string GetValue(string buffer)
        {
            string value;

            if (buffer.StartsWith("\t\t"))
                value = buffer.Substring(2);
            else
                value = buffer.Substring(buffer.IndexOf(":") + 1);

            return value;
        }
    }
}
