﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;
using GM.Models;
using GM.Utils.Index;

namespace GM.Utils
{
    public class FileManager
    {
        private IndexManager indexManager;
        private DishFileManager dishManager;
        private MenuFileManager menuManager;

        public static string Path { get { return ConfigurationSettings.AppSettings.Get("mainPath"); } }
        public List<string> IndexValues { get { return indexManager.IndexValues; } }
        public List<string> MenusList { get { return indexManager.MenusList; } }
        public List<string> FirstDishesList { get { return indexManager.FirstDishesList; } }
        public List<string> MainDishesList { get { return indexManager.MainDishesList; } }
        public List<string> DesertsList { get { return indexManager.DesertsList; } }
        public List<string> DrinksList { get { return indexManager.DrinksList; } }

        public FileManager()
        {
            indexManager = new IndexManager();
            dishManager = new DishFileManager() { FileManager = this };
            menuManager = new MenuFileManager() { FileManager = this };
        }

        public MenuItem ReadMenuItem(string name)
        {
            long id = GetIdForName(name);
            if (MenusList.Contains(name)) return ReadMenu(id);
            else return ReadDish(id);
        }

        public void WriteDish(Dish dish) { indexManager.AddItem(dish); dishManager.Write(dish); }
        public Dish ReadDish(long id) { return dishManager.Read(id) as Dish; }
        public void EditDish(Dish editingDish) { indexManager.EditItem(editingDish); dishManager.Write(editingDish); }

        public void WriteMenu(Menu menu) { indexManager.AddItem(menu); menuManager.Write(menu); }
        public Menu ReadMenu(long id) { return menuManager.Read(id) as Menu; }
        public void EditMenu(Menu editingMenu) { indexManager.EditItem(editingMenu); menuManager.Write(editingMenu); }

        public void WriteIndex() { indexManager.WriteIndex(); }
        public long GetIdForName(string name) { return indexManager.IdForName(name); }
        public string GetNameForId(long id) { return indexManager.GetNameForId(id); }
        public byte GetByteForTypes(string name) { return indexManager.GetByteForTypes(name); }
        public List<DishType> GetTypesForByte(byte byteCode) { return indexManager.GetTypesForByte(byteCode); }

    }
}
