﻿using GM.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Utils
{
    public class DishFileManager : SpecializedManager
    {
        private string name;
        private decimal price;
        private List<DishType> dishTypes;
        private List<string> ingredients;

        public DishFileManager()
        {
            dishTypes = new List<DishType>();
            ingredients = new List<string>();
        }

        override public void Write(MenuItem dish)
        {
            try
            {
                using (Writer = File.CreateText(FileManager.Path + dish.Id))
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine("Plat");
                    builder.AppendLine("\tId:" + dish.Id);
                    builder.AppendLine("\tName:" + dish.Name);
                    builder.AppendLine("\tPrice:" + dish.Price);
                    builder.AppendLine("\tAvailableDishType:");
                    foreach (DishType dishType in (dish as Dish).DishTypes)
                        builder.AppendLine("\t\t" + DishTypeString.GetString(dishType));

                    builder.AppendLine("\tIngredients:");

                    foreach (string ingredient in (dish as Dish).Ingredients)
                        builder.AppendLine("\t\t" + ingredient);

                    Writer.Write(builder.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        override public MenuItem Read(long id)
        {
            try
            {
                using (Reader = File.OpenText(FileManager.Path + id))
                {
                    SetValues();
                    return new Dish(id, name, price, ingredients, dishTypes);;
                }
            }
            catch (Exception e) { throw e; }
        }

        private void SetValues()
        {
            ClearLists();

            string buffer = Reader.ReadLine();
            while (buffer != null)
            {
                if (buffer.Contains("\tName:"))
                {
                    name = GetValue(buffer);
                    buffer = Reader.ReadLine();
                }
                else if (buffer.Contains("\tPrice:"))
                {
                    price = decimal.Parse(GetValue(buffer));
                    buffer = Reader.ReadLine();
                }
                else if (buffer.Contains("\tAvailableDishType:"))
                {
                    buffer = Reader.ReadLine();
                    while (buffer != null && buffer.StartsWith("\t\t"))
                    {
                        dishTypes.Add(DishTypeString.GetDishType(GetValue(buffer)));
                        buffer = Reader.ReadLine();
                    }
                }
                else if (buffer.Contains("\tIngredients:"))
                {
                    buffer = Reader.ReadLine();
                    while (buffer != null && buffer.StartsWith("\t\t"))
                    {
                        ingredients.Add(GetValue(buffer));
                        buffer = Reader.ReadLine();
                    }
                }
                else if (buffer != null)
                    buffer = Reader.ReadLine();
            }
        }

        private void ClearLists()
        {
            dishTypes.Clear();
            ingredients.Clear();
        }
    }
}
