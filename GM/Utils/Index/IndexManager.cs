﻿using GM.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.Utils.Index
{
    public class IndexManager
    {
        private string path;
        private IDictionary<string, long> indexValues;

        private IDictionary<DishType, List<string>> namesForType;
        private List<string> menusList;

        public List<string> IndexValues { get { return indexValues.Keys.ToList(); } }
        public List<string> MenusList { get { return menusList; } }
        public List<string> FirstDishesList { get { return namesForType[DishType.FirstDish]; } }
        public List<string> MainDishesList { get { return namesForType[DishType.MainDish]; } }
        public List<string> DesertsList { get { return namesForType[DishType.Dessert]; } }
        public List<string> DrinksList { get { return namesForType[DishType.Drink]; } }

        public IndexManager()
        {
            path = FileManager.Path + "index";

            indexValues = new SortedDictionary<string, long>();
            namesForType = new Dictionary<DishType, List<string>>();
            menusList = new List<string>();

            foreach (DishType dishType in Enum.GetValues(typeof(DishType)))
                namesForType.Add(dishType, new List<string>());

            InitIndex();

        }

        private void InitIndex()
        {
            if (File.Exists(path))
            {
                string name;
                byte availableDishTypes;
                long id;
                try
                {
                    using (BinaryReader chanel = new BinaryReader(File.Open(path, FileMode.Open), Encoding.UTF8))
                    {
                        chanel.BaseStream.Seek(0, SeekOrigin.Begin);
                        do
                        {
                            //Read byte for dish type
                            availableDishTypes = chanel.ReadByte();
                            //Read name on 100 chars and trim it to remove additional blank spaces
                            name = new String(chanel.ReadChars(100)).Trim();
                            //Read id
                            id = chanel.ReadInt64();

                            //Add name and id in the dictionnary
                            indexValues.Add(name, id);

                            //If code is 0 then add to menu
                            if (availableDishTypes == 0)
                                menusList.Add(name);
                            //Else we add the dishes to the right lists
                            else
                            {
                                List<DishType> types = GetTypesForByte(availableDishTypes);
                                foreach (DishType type in types)
                                    namesForType[type].Add(name);
                            }

                        } while (chanel.BaseStream.Position < chanel.BaseStream.Length);
                    }
                }
                catch (IOException e)
                {

                }
            }
        }

        private void DeleteItem(MenuItem menuItem)
        {
            foreach (KeyValuePair<string, long> item in indexValues)
                if (item.Value == menuItem.Id)
                {
                    indexValues.Remove(item);
                    break;
                }
        }

        public void EditItem(Dish editingDish)
        {
            DeleteItem(editingDish);
            AddItem(editingDish);
        }

        public void EditItem(Menu editingMenu)
        {
            DeleteItem(editingMenu);
            AddItem(editingMenu);
        }

        public void AddItem(Dish dish)
        {
            indexValues.Add(dish.Name, dish.Id);

            if (dish.DishTypes.Contains(DishType.FirstDish))
                namesForType[DishType.FirstDish].Add(dish.Name);
            if (dish.DishTypes.Contains(DishType.MainDish))
                namesForType[DishType.MainDish].Add(dish.Name);
            if (dish.DishTypes.Contains(DishType.Dessert))
                namesForType[DishType.Dessert].Add(dish.Name);
            if (dish.DishTypes.Contains(DishType.Drink))
                namesForType[DishType.Drink].Add(dish.Name);
        }

        public void AddItem(Menu menu)
        {
            indexValues.Add(menu.Name, menu.Id);
            menusList.Add(menu.Name);
        }

        public void WriteIndex()
        {
            try
            {
                using (BinaryWriter chanel = new BinaryWriter(File.Open(path, FileMode.OpenOrCreate)))
                {
                    chanel.Seek(0, SeekOrigin.Begin);
                    foreach (KeyValuePair<string, long> item in indexValues)
                    {
                        //Writing types of dish (or 0000 if menu)
                        chanel.Write(GetByteForTypes(item.Key));
                        //Writing name of item
                        chanel.Write(Encoding.UTF8.GetBytes(item.Key));
                        //Writing blank spaces after name to make entry 100 chars
                        chanel.Write(Encoding.UTF8.GetBytes(new string(' ', 100 - item.Key.Length)));
                        //Writing id
                        chanel.Write(item.Value);
                    }
                }
            }
            catch (IOException e)
            {

            }
        }

        public long IdForName(string name)
        {
            return indexValues[name];
        }

        public string GetNameForId(long id)
        {
            foreach (KeyValuePair<string, long> values in indexValues)
                if (values.Value == id)
                    return values.Key;

            return null;
        }

        public byte GetByteForTypes(string name)
        {
            byte typeByte = 0;

            if (namesForType[DishType.FirstDish].Contains(name))
                typeByte += 8;
            if (namesForType[DishType.MainDish].Contains(name))
                typeByte += 4;
            if (namesForType[DishType.Dessert].Contains(name))
                typeByte += 2;
            if (namesForType[DishType.Drink].Contains(name))
                typeByte += 1;

            return typeByte;
        }

        public List<DishType> GetTypesForByte(byte byteCode)
        {
            List<DishType> types = new List<DishType>();

            if (byteCode - 8 >= 0) { types.Add(DishType.FirstDish); byteCode -= 8; }
            if (byteCode - 4 >= 0) { types.Add(DishType.MainDish); byteCode -= 4; }
            if (byteCode - 2 >= 0) { types.Add(DishType.Dessert); byteCode -= 2; }
            if (byteCode - 1 >= 0) { types.Add(DishType.Drink); byteCode -= 1; }

            return types;
        }
    }
}
