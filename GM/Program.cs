﻿using GM.Core;
using GM.Menus.Controllers;
using GM.Menus.Views;
using GM.Models;
using GM.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace GM
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SetupApplication();
            Application.Run(SetupControllers());
        }

        private static Form SetupControllers()
        {
            FileManager fileManager = new FileManager();

            ApplicationController appController = new ApplicationController();
            MainWindow mainWindow = new MainWindow();

            MainWindowController mainWindowController = new MainWindowController() { Window = mainWindow, FileManager = fileManager };
            appController.Register(mainWindowController);

            EditDishController editDishController = new EditDishController() { Window = new EditDishWindow(), FileManager = fileManager };
            appController.Register(editDishController);
            
            EditMenuController editMenuController = new EditMenuController() { Window = new EditMenuWindow(), FileManager = fileManager };
            appController.Register(editMenuController);

            TablesController tablesController = new TablesController() { Window = new TablesWindow(), FileManager = fileManager };
            appController.Register(tablesController);

            OrderController orderController = new OrderController() { Window = new OrderWindow(), FileManager = fileManager };
            orderController.orderCreated += tablesController.RequestWindowUpdate;
            appController.Register(orderController);

            OrderHistoryController orderHistoryController = new OrderHistoryController() { Window = new OrderHistoryWindow(), FileManager = fileManager };
            appController.Register(orderHistoryController);

            return mainWindow;
        }

        private static void SetupApplication()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        }
    }
}
