﻿using GM.Menus.Views.Models;
using GM.Models.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.DataSources
{
    public interface IOrderMapper
    {
        Order GetById(long id);
        void Add(Order o);
        void CloseOrder(Table table);
    }
}
