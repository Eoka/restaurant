﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GM.Models.Orders;
using GM.Menus.Views.Models;

namespace GM.DataSources.EntityFramework
{
    public class EFOrderMapper : EFMapper, IOrderMapper
    {
        public EFOrderMapper() : base() { }
        public EFOrderMapper(string connectionString) : base(connectionString) { }

        public void Add(Order o)
        {
            if (o.Id != 0)
            {
                throw new ArgumentException("New Entity should have 0 id");
            }
            using (RestaurantDataContext context = new RestaurantDataContext(ConnectionString))
            {
                context.Orders.Add(o);
                context.SaveChanges();
            }
        }

        public void CloseOrder(Table table)
        {
            using (RestaurantDataContext context = new RestaurantDataContext(ConnectionString))
            {
                Order order = context.Orders.First((Order o) => o.TableNumber == table.TableId && o.OrderClosing < o.OrderOpening);
                order.OrderClosing = DateTime.Now;
                context.SaveChanges();
            }
        }

        public Order GetById(long id)
        {
            using (RestaurantDataContext context = new RestaurantDataContext(ConnectionString))
            {
                return context.Orders.Include("OrderLines").First((Order o) => o.Id == id);
            }
        }

        public Historic GetTodayHistoric(Historic historic)
        {
            using (RestaurantDataContext context = new RestaurantDataContext(ConnectionString))
            {
                float total = 0;
                float averageTime = 0;
                List<Order> orders = context.Orders.Include("OrderLines").Where(o => o.OrderOpening > DateTime.Today && o.TableNumber == historic.Table).ToList();

                if (orders != null)
                {
                    foreach (Order o in orders)
                    {
                        total += o.TotalPrice;
                        averageTime += (float)(o.OrderClosing - o.OrderOpening).TotalMinutes;
                    }
                    historic.OrderAmount = orders.Count;
                    historic.TotalPrice = total;
                    historic.AverageTime = (int)(averageTime / (historic.OrderAmount > 0 ? historic.OrderAmount : 1));
                }
            }

            return historic;
        }
    }
}
