﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.DataSources.EntityFramework
{
    public abstract class EFMapper
    {
        public string ConnectionString { get; private set; }

        public EFMapper(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public EFMapper() : this("Restaurant") { }
    }
}
