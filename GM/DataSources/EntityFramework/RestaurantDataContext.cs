﻿using GM.Models.Orders;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GM.DataSources.EntityFramework
{
    public class RestaurantDataContext : DbContext
    {
        public RestaurantDataContext() : this("Restaurant-Test") { }

        public RestaurantDataContext(string connectionString) : base(connectionString)
        {

        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                .HasKey(o => o.Id);
            modelBuilder.Entity<Order>()
                .HasMany(o => o.OrderLines)
                .WithRequired();
            modelBuilder.Entity<Order>()
                .Ignore(o => o.TotalPrice);

            modelBuilder.Entity<OrderLine>()
                .HasKey(ol => ol.Id);
            modelBuilder.Entity<OrderLine>()
                .Ignore(ol => ol.TotalPrice)
                .Ignore(ol => ol.Item);
        }
    }
}
