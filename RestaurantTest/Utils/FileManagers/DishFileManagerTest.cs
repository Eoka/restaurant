﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GM.Utils;
using System.IO;
using GM.Models;

namespace RestaurantTest
{
    /// <summary>
    /// Summary description for DishFileManagerTest
    /// </summary>
    [TestClass]
    public class DishFileManagerTest
    {
        private FileManager fileManager = new FileManager();

        [TestMethod]
        public void DishFileManagerShouldWorkWithAReader()
        {
            DishFileManager dishFileManager = new DishFileManager() { FileManager = fileManager };
            TextReader reader = new StringReader("");
            dishFileManager.Reader = reader;

            Assert.AreEqual(reader, dishFileManager.Reader, "The reader should be the one defined in the class");
        }

        [TestMethod]
        public void DishNameShouldBeTheOneInTheFile() // ce test ne passe pas
        {

            DishFileManager dishFileManager = new DishFileManager() { FileManager = fileManager };
            TextReader reader = new StringReader("Name");
            dishFileManager.Reader = reader;

            MenuItem item = dishFileManager.Read(0);

            Assert.AreEqual("Name", dishFileManager.Reader, "Dish name should be Name");


        }
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void FileNotFoundOnRead()
        {
            DishFileManager dishFileManager = new DishFileManager() { FileManager = fileManager };
            dishFileManager.Read(0);
        }
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void FileNotFoundOnWrite() // ce test ne passe pas
        {
            DishFileManager dishFileManager = new DishFileManager() { FileManager = fileManager };
            dishFileManager.Write(null);
        }



    }
}
