﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GM.Utils;
using System.IO;

namespace RestaurantTest.Utils.FileManagers
{
    [TestClass]
    public class MenuFileManagerTest
    {
        private FileManager fileManager = new FileManager();

        [TestMethod]
        public void MenuFileManagerShouldWorkWithAReader()
        {
            MenuFileManager menuFileManager = new MenuFileManager() { FileManager = fileManager };
            TextReader reader = new StringReader("");
            menuFileManager.Reader = reader;

            Assert.AreEqual(reader, menuFileManager.Reader, "The reader should be the one defined in the class");
        }
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void FileNotFoundOnRead()
        {
            MenuFileManager menuFileManager = new MenuFileManager() { FileManager = fileManager };
            menuFileManager.Read(0);
        }
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void FileNotFoundOnWrite() // ce test ne passe pas
        {
            MenuFileManager menuFileManager = new MenuFileManager() { FileManager = fileManager };
            menuFileManager.Write(null);
        }

    }
}
