﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GM.DataSources.EntityFramework;
using GM.Models.Orders;
using System.Data.Entity;

namespace RestaurantTest.DataSources.EntityFramework
{
    [TestClass]
    public class EFMapperTests
    {
        private const string connectionString = "RestaurantTest";

        private DatabaseChecker dbChecker;

        private RestaurantDataContext context;
        private EFOrderMapper orderMapper;

        [ClassInitialize]
        public static void SetupDataBase(TestContext context)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<RestaurantDataContext>());
        }

        [TestInitialize]
        public void Setup()
        {
            context = new RestaurantDataContext(connectionString);
            dbChecker = new DatabaseChecker(context.Database.Connection.ConnectionString);
            orderMapper = new EFOrderMapper(connectionString);
        }

        [TestMethod]
        public void TestingOrderTableCreation()
        {
            Order o = new Order();
            orderMapper.Add(o);

            dbChecker.AssertTableExists("Orders");
            dbChecker.AssertTableHasColumns("Id", "TableNumber", "OrderOpening", "OrderClosing");
        }

        [TestMethod]
        public void TestingOrderLineTableCreation()
        {
            Order o = new Order();
            OrderLine ol = new OrderLine();
            o.OrderLines.Add(ol);
            orderMapper.Add(o);

            dbChecker.AssertTableExists("OrderLines");
            dbChecker.AssertTableHasColumns("Id", "ItemAdress", "Quantity", "UnitPrice","Order_Id");
        }

        [TestMethod]
        public void AddingAnOrderUpdatesHisId()
        {
            Order o = new Order();
            orderMapper.Add(o);

            Assert.AreNotEqual(0, o.Id, "Expected Id should differ from 0");
        }

        [TestMethod]
        public void AddingAnOrderLineUpdatesHisId()
        {
            Order o = new Order();
            OrderLine ol = new OrderLine();

            o.OrderLines.Add(ol);
            orderMapper.Add(o);

            Assert.AreNotEqual(0, ol.Id, "Expected Id should differ from 0");
        }

        [TestMethod]
        public void OrderEntitiesAreFetchable()
        {
            Order o = new Order();
            o.TableNumber = 6;
            orderMapper.Add(o);

            Order fetched = orderMapper.GetById(o.Id);

            Assert.AreEqual(6, fetched.TableNumber, "Expected TableNumber is 6, value fetched = " + fetched.TableNumber);
        }

        [TestMethod]
        public void OrderLineEntitiesAreFetchable()
        {
            Order o = new Order();
            OrderLine ol = new OrderLine() { ItemAdress = 6 };

            o.OrderLines.Add(ol);
            orderMapper.Add(o);

            Order fetched = orderMapper.GetById(o.Id);

            Assert.AreEqual(6, fetched.OrderLines[0].ItemAdress, "Expected TableNumber is 6, value fetched = " + fetched.OrderLines[0].ItemAdress);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InsertingOrderEntityTwiceThorwsException()
        {
            Order o = new Order();
            o.TableNumber = 6;

            orderMapper.Add(o);
            orderMapper.Add(o);
        }
    }
}
