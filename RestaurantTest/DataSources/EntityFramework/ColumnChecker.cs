﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RestaurantTest
{
    public class ColumnChecker
    {
        public static readonly Func<string, Action<DataRow>> ColumnType = s => dr => Assert.AreEqual(s.ToLower(), dr["data_type"]);
        public static readonly Func<int?, Action<DataRow>> StringLength = l => dr => Assert.AreEqual(l, dr["character_maximum_length"]);
        
        public static readonly Action<DataRow> IsNullable = dr => Assert.AreEqual("YES",dr["is_nullable"]);
        public static readonly Action<DataRow> IsNotNullable = dr => Assert.AreEqual("NO", dr["is_nullable"]);
    }
}