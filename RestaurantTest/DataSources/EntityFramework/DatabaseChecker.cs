﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Data.SqlClient;

namespace RestaurantTest
{
    public class DatabaseChecker
    {
        private string connectionString;

        public DatabaseChecker(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void AssertTableExists(string tableName)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                var dTable = con.GetSchema("TABLES", new string[] { null, null, tableName });

                Assert.IsTrue(dTable.Rows.Count > 0);
            }
        }

        public void AssertTableHasColumns(String tableName, params string[] columnNames)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                foreach (string column in columnNames)
                {
                    var dTable = con.GetSchema("COLUMNS", new string[] { null, null, tableName, column });
                    Assert.IsTrue(dTable.Rows.Count > 0);
                }
            }
        }

        public void AssertColumnHasProperties(string tableName, string columnName, params Action<DataRow>[] checkers)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                DataTable info = con.GetSchema("COLUMNS", new string[] { null, null, tableName, columnName });
                foreach (var checker in checkers)
                {
                    checker.Invoke(info.Rows[0]);
                }
            }
        }

        internal void AssertTupleExists(string p1, string p2, long p3)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                SqlCommand command = con.CreateCommand();
                command.CommandText = string.Format("SELECT * FROM {0} where {1} = @val",p1,p2);
                command.Parameters.Add(new SqlParameter() 
                { 
                    ParameterName="@val", 
                    DbType = DbType.Int64, 
                    Value = p3
                });

                SqlDataReader result = command.ExecuteReader();
                Assert.IsTrue(result.Read());
            }
        }
    }
}
